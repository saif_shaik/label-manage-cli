var inquirer = require("inquirer");
var mysql = require("mysql");
var fs = require("fs");
var Spinner = require('cli-spinner').Spinner;
var chalk = require('chalk');

var spinner = new Spinner('processing.. %s');
spinner.setSpinnerString('|/-\\');
var labelKeyQueriesArr = '';
var labelDataQueriesArr = '';


var con = mysql.createConnection({
  host:
    "avancedevqa.c0h4rminjcqh.us-east-1.rds.amazonaws.com",
  user: "root",
  port: 3306,
  password: "aVance54239",
});

function connection(userName, password) {
  return new Promise((resolve, reject) => {
    mysql.createConnection({
      host:
        "avancedevqa.c0h4rminjcqh.us-east-1.rds.amazonaws.com",
      user: userName,
      port: 3306,
      password: password,
    }).connect(function (err) {
      if (err) {
        console.log();
        console.log('Please check your password and try again')
        reject(err);
      }
      resolve("connected");
    });
  });
}

async function main() {
    printWelcomeMessage();
    const authenticationEnquirer = inquirer.prompt([
      /* Pass your questions in here */
      {
        type: "input",
        message: "User Name:",
        name: "userName",
      },
      {
        type: "password",
        message: "password",
        name: "password",
      },
    ]);
    const { userName, password} = await getResponseValues(authenticationEnquirer);
    spinner.setSpinnerTitle('connecting to DB');
    spinner.start();
    await connection(userName, password).then((val) => {
      spinner.stop();
      printConnectedToDBMessage();
    });
    runEnquiry();
    console.log()
    console.log();
}

function printWelcomeMessage() {
  console.log()
  console.log(chalk.green('*--------------------------------------*'))
  console.log(chalk.green('*  Welcome to Avance Label Management  *'));
  console.log(chalk.green('*--------------------------------------*'))
  console.log();
}

function printConnectedToDBMessage() {
  console.log()
  console.log(chalk.green('*--------------------------*'))
  console.log(chalk.green('*   Connected to DB (^_^)  *'));
  console.log(chalk.green('*--------------------------*'))
  console.log();
}

function labelCreatedMessage() {
  console.log();
  console.log(chalk.green('*---------------------------------------------------------------------*'))
  console.log(chalk.green('*   Label Created Successfully and Queries Added in sql-queries file  *'));
  console.log(chalk.green('*---------------------------------------------------------------------*'));
  console.log();
  console.log();
}

async function runEnquiry() {
  const inquirerPromise = inquirer.prompt([
    /* Pass your questions in here */
    {
      type: "checkbox",
      message: 'In which Enviornments do you want to create labels ?',
      name: 'envs',
      choices: ['DEV']
    },
    {
      type: "input",
      message: "Please Provide the Label key",
      name: "labelKey",
    },
    {
      type: "input",
      message: "Please Provide the Label Data(What you want to show on UI)",
      name: "labelData",
    },
  ]);

  const { labelKey, labelData, envs } = await getResponseValues(inquirerPromise);
  spinner.setSpinnerTitle('Creating label');
  spinner.start();

  if(envs.includes('DEV')) {
    insertQueries('DEV', labelKey, labelData);
  }
 
  spinner.stop();
  labelCreatedMessage();
  runEnquiry()
}

function getResponseValues(inquirerPromise) {
  return new Promise((resolve, reject) => {
    inquirerPromise.then((answers) => {
      resolve(answers);
    });
  });
}

function insertLabelKey(env, labelKey) {
  return new Promise((resolve, reject) => {
    const query = `INSERT INTO AvanceManager_${env}.LabelKey (LabelKey) VALUES ('${labelKey}');`;
    con.query(query, (err, res) => {
      if (err) throw err;
      labelKeyQueriesArr = query.replace(`AvanceManager_${env}.`, '');
      resolve(res);
    });
  });
}

function insertLabelData(env, labelKey, labelData) {
  return new Promise((resolve, reject) => {
    const query = `INSERT INTO AvanceManager_${env}.LabelInfo (LabelKeyId, LabelInfo, TenantId, Created, LastUpdated, LookupValueId) VALUES ((
            select LabelKeyId from AvanceManager_${env}.LabelKey where LabelKey='${labelKey}'), 
            '${labelData}', 
            '1', curdate(), curdate(), '1');`;
    con.query(query, (err, res) => {
      if (err) throw err;
      labelDataQueriesArr = query.replace(`AvanceManager_${env}.`, '');
      resolve(res);
    });
  });
}

async function insertQueries(env, labelKey, labelData) {
  await insertLabelKey(env,labelKey);
  console.log();
  await addQueriesToFile(labelKeyQueriesArr + '\n', 'query for label-key added to sql-queries.txt file!')
  await insertLabelData(env, labelKey, labelData);
  await addQueriesToFile(labelDataQueriesArr + '\n \n', 'query for label-data added to sql-queries.txt file!')
}

function addQueriesToFile(content, consoleText) {
  return new Promise((resolve, reject) => {
    fs.appendFile('sql-queries/sql-queries.txt', content, function (err) {
      if (err) throw err;
      resolve(content)
    });
  })
}

main();
